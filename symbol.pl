programa([begin|L]):- append(L1,[end],L), instrucciones(L1).
instrucciones(L):- instruccion(L).
instrucciones(L):- append(I, [;|L1], L), instruccion(I), instrucciones(L1).
instruccion([if,X,=,Y,then|L]):- variable(X), variable(Y), lastElement(L, L1, endif), append(L2,[else|L3],L1), instrucciones(L2), instrucciones(L3).
instruccion([X,=,Y,+,Z]):- variable(X), variable(Y), variable(Z).
variable(x).
variable(y).
variable(z).

lastElement([M], [], M).
lastElement([X|Xs], [X|Ys], M):- lastElement(Xs, Ys, M).
