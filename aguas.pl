nat(1).
nat(N):- nat(N1), N is N1+1.

camino( E,E, C,C ).
camino( EstadoActual, EstadoFinal, CaminoHastaAhora, CaminoTotal ):-
    unPaso( EstadoActual, EstSiguiente ),
    \+member(EstSiguiente,CaminoHastaAhora),
    camino( EstSiguiente, EstadoFinal, [EstSiguiente|CaminoHastaAhora], CaminoTotal ).

solucionOptima:-
    nat(N),
    write(N),
    camino([0,0],[0,4],[[0,0]],C),
    length(C,N),
    write(C).

unPaso([_,B],[5,B]).
unPaso([A,_],[A,8]).
unPaso([A,B],[C,D]):- D is min(A+B,8), C is max(0,A+B-8).
unPaso([A,B],[C,D]):- C is min(A+B,5), D is max(0,B+A-5).
unPaso([_,B],[0,B]).
unPaso([A,_],[A,0]).


camino2( E,E, C,C ).
camino2( EstadoActual, EstadoFinal, CaminoHastaAhora, CaminoTotal ):-
    unPaso2( EstadoActual, EstSiguiente ),
    \+member(EstSiguiente,CaminoHastaAhora),
    camino2( EstSiguiente, EstadoFinal, [EstSiguiente|CaminoHastaAhora], CaminoTotal ).

solucionMisioneros:-
    nat(N),
    camino2([3,3,0], [0,0,1], [[3,3,0]], C),
    length(C,N),
    write(C).

% de 1 a 2
unPaso2([MB,C,0],[MA,C,1]):- MA is MB-1, valid(MA, C).
unPaso2([MB,C,0],[MA,C,1]):- MA is MB-2, valid(MA, C).
unPaso2([M,CB,0],[M,CA,1]):- CA is CB-1, valid(M, CA).
unPaso2([M,CB,0],[M,CA,1]):- CA is CB-2, valid(M, CA).
unPaso2([MB,CB,0],[MA,CA,1]):- MA is MB-1, CA is CB-1, valid(MA, CA).

% de 2 a 1
unPaso2([MB,C,1],[MA,C,0]):- MA is MB+1, valid(MA, C).
unPaso2([MB,C,1],[MA,C,0]):- MA is MB+2, valid(MA, C).
unPaso2([M,CB,1],[M,CA,0]):- CA is CB+1, valid(M, CA).
unPaso2([M,CB,1],[M,CA,0]):- CA is CB+2, valid(M, CA).
unPaso2([MB,CB,1],[MA,CA,0]):- MA is MB+1, CA is CB+1, valid(MA, CA).

valid(M,C):- M >= 0, M =< 3, C >= 0, C =< 3, (M = 0; M >= C), (M = 3; M =< C).

